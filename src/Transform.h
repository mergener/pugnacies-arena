#pragma once

#include "Vector2.h"

class Transform
{
public:
	const Vector2& GetPosition() const;
	void SetPosition(const Vector2& pos);

	const Vector2& GetScale() const;
	void SetScale(const Vector2& scale);

private:
	Vector2 position;
	Vector2 scale;
};