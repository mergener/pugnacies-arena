#include "Assets.h"

namespace Assets
{
	namespace Images
	{
		static void Load()
		{

		}
	} // Images

	namespace Fonts
	{
		FontAsset* Arial16;
		FontAsset* Arial24;

		static void Load()
		{
			Arial16 = new FontAsset("C:/Windows/Fonts/arial.ttf", 16);
			Arial24 = new FontAsset("C:/Windows/Fonts/arial.ttf", 24);
		}
	} // Fonts

	void Load()
	{
		Images::Load();
		Fonts::Load();
	}
} // Assets