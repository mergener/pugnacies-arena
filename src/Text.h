#pragma once

#include <string>
#include <stdexcept>

#include "SDL.h"
#include "Font.h"

class TextCreationException : public std::runtime_error
{
public:
	TextCreationException(const char* msg);
};

class TextHandle
{
public:
	enum class AlignMode
	{
		Left, Center, Right
	};
	AlignMode alignment = AlignMode::Left;

	TextHandle(const std::string& textHandle, const FontAsset* font);
	TextHandle(const TextHandle& other);
	TextHandle& operator=(const TextHandle& other);
	~TextHandle();

	int GetWidth() const;
	int GetHeight() const;
	
	SDL_Texture* GetTexture() const;
	const std::string& GetString() const;
	const FontAsset* GetFont() const;

private:
	std::string textHandle;

	const FontAsset* font;
	SDL_Texture* texture;

	int width;
	int height;
};