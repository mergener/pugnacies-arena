#include "GameState.h"

#include "Game States/MainMenu.h"

GameState* GameState::currentState = nullptr;

void GameState::SetCurrent(GameState* state) 
{
	if (currentState)
	{
		currentState->OnEnd();
	}
	currentState = state;
	currentState->Start();
}

void GameState::Start()
{

}

void GameState::OnUpdate(float dt)
{

}

void GameState::OnRender()
{

}

void GameState::OnLateUpdate(float dt)
{

}

void GameState::OnEnd()
{

}

void GameState::OnApplicationQuit()
{

}

void GameState::OnKeyDown(SDL_Keycode key, bool repeat)
{

}

void GameState::OnMouseDown(Uint8 button)
{

}