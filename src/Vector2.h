#pragma once

#include <cmath>
#include <string>
#include <type_traits>

template<typename T>
struct _Vec2
{
	static_assert(std::is_arithmetic<T>::value && (std::is_integral<T>::value || std::is_floating_point<T>::value), "Invalid _Vec2 template parameter.");

	T x;
	T y;

	inline _Vec2<T> operator+(const _Vec2<T>& other) const
	{
		return _Vec2<T>(x + other.x, y + other.y);
	}

	inline _Vec2<T> operator-(const _Vec2<T>& other) const
	{
		return _Vec2<T>(x - other.x, y - other.y);
	}

	inline _Vec2<T> operator +=(const _Vec2<T>& other) const
	{
		return *this + other;
	}

	inline _Vec2<T> operator -=(const _Vec2<T>& other) const
	{
		return *this - other;
	}

	template <typename T2>
	inline _Vec2<T> operator*(T2 amt) const
	{
		static_assert(std::is_arithmetic<T>::value && (std::is_integral<T>::value || std::is_floating_point<T>::value), "Invalid _Vec2 template parameter.");
		return _Vec2<T>(static_cast<T>(static_cast<T2>(x) * amt), static_cast<T>(static_cast<T2>(y) * amt));
	}

	template <typename T2>
	inline _Vec2<T> operator/(T2 amt) const
	{
		static_assert(std::is_arithmetic<T>::value && (std::is_integral<T>::value || std::is_floating_point<T>::value), "Invalid _Vec2 template parameter.");
		return _Vec2<T>(static_cast<T>(static_cast<T2>(x) / amt), static_cast<T>(static_cast<T2>(y) / amt));
	}

	template <typename T2>
	inline _Vec2<T>& operator *=(T2 amt) const
	{
		return *this * amt;
	}

	template <typename T2>
	inline _Vec2<T>& operator /=(T2 amt) const
	{
		return *this / amt;
	}

	inline static T Dot(const _Vec2<T>& a, const _Vec2<T>& b)
	{
		return a.x * b.x + a.y * b.y;
	}

	inline static _Vec2<T> Lerp(const _Vec2<T>& a, const _Vec2<T>& b, float t)
	{
		return _Vec2<T>(a.x + (b.x - a.x)*t, a.y + (b.y - a.y)*t);
	}

	inline float GetMagnitude() const
	{
		return std::sqrt(static_cast<float>(x * x + y * y));
	}

	inline _Vec2<T> GetNormalized() const
	{
		return _Vec2<T>(x, y) / GetMagnitude();
	}

	inline void Normalize() const
	{
		float magnitude = GetMagnitude();
		x = static_cast<T>(static_cast<float>(x) / magnitude);
		y = static_cast<T>(static_cast<float>(y) / magnitude);
	}

	inline _Vec2<T>(T x = 0, T y = 0)
		: x(x), y(y)
	{
	}

	template<typename T2>
	inline _Vec2<T>(const _Vec2<T2>&& other)
		: x(static_cast<T>(other.x)), y(static_cast<T>(other.y))
	{
	}

	template<typename T2>
	inline _Vec2<T>(const _Vec2<T2>& other)
		: x(static_cast<T>(other.x)), y(static_cast<T>(other.y))
	{
	}

	template <typename T2>
	inline _Vec2<T>& operator=(const _Vec2<T2>&& other)
	{
		x = static_cast<T>(other.x);
		y = static_cast<T>(other.y);
		return *this;
	}

	template <typename T2>
	inline _Vec2<T>& operator=(const _Vec2<T2>& other)
	{
		x = static_cast<T>(other.x);
		y = static_cast<T>(other.y);
		return *this;
	}
};

using Vector2 = _Vec2<float>;
using Vector2Int = _Vec2<int>;
using Vector2Double = _Vec2<double>;
using Vector2String = _Vec2<std::string>;