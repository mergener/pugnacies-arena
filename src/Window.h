#pragma once

#include <SDL.h>

#include <stdexcept>

#include "Vector2.h"

class Window
{
public:
	inline static Window* GetInstance() { return instance; }
	inline static void SetMain(Window* window) { instance = window; }

	inline SDL_Window* GetHandle() const { return window; }
	Vector2Int GetBounds() const;
	void SetDimensions(int w, int h);
	void SetDimensions(const Vector2Int& dim);

	Window(const char* title, int x, int y, int w, int h, Uint32 flags);
	Window(const Window& other) = delete;

private:
	static Window* instance;
	SDL_Window* window;
};

class WindowCreationException : std::runtime_error
{
public:
	inline WindowCreationException(const char* what)
		: std::runtime_error(what) { }
};