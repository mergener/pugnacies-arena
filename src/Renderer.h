#pragma once

#include <SDL.h>

#include "ImageAsset.h"
#include "Window.h"
#include "Text.h"

#define MainRenderer (Renderer::GetInstance())

class Renderer
{
public:
	inline static Renderer* GetInstance() { return instance; }
	inline static void SetMain(Renderer* newMainRenderer) { instance = newMainRenderer; }
	inline SDL_Renderer* GetHandle() { return renderer; }

	const Window& GetWindow() const;
	
	void DrawRectangle(int x, int y, int w, int h);
	void DrawCircle(int x, int y, int radius);
	
	void FillRectangle(int x, int y, int w, int h);
	void FillCircle(int x, int y, int radius);

	void DrawImage(const ImageAsset& img, int x, int y);
	void DrawImage(const ImageAsset& img, int x, int y, float sx, float sy);
	void DrawImage(const ImageAsset& img, int x, int y, int qx, int qy, int qw, int qh);
	void DrawImage(const ImageAsset& img, int x, int y, float sx, float sy, int qx, int qy, int qw, int qh);
	void DrawText(const TextHandle& textHandle, int x, int y);

	SDL_Color GetColor() const;
	SDL_Color GetBackgroundColor() const;
	
	void SetColor(const SDL_Color& color);
	void SetColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a = 255);
	
	void SetBackgroundColor(Uint8 r, Uint8 g, Uint8 b);

	void RenderPresent();

	Renderer(Window& window, int index, Uint32 flags, SDL_BlendMode blendMode = SDL_BLENDMODE_BLEND);

private:
	static Renderer* instance;
	Window* window;
	SDL_Renderer* renderer;
	SDL_Color color;
	SDL_Color backgroundColor = { 0, 0, 0, 255 };
};

class RendererCreationException : std::runtime_error
{
public:
	inline RendererCreationException(const char* what)
		: std::runtime_error(what) { }
};