#include "ImageAsset.h"

#include <iostream>

#include "Renderer.h"

ImageAsset::ImageAsset(Renderer& renderer, const std::string& path)
	: ImageAsset(renderer, path.c_str())
{
}

ImageAsset::ImageAsset(Renderer& renderer, const char* path)
{
	this->path = path;
	SDL_Surface* surface = IMG_Load(path);
	
	if (!surface)
	{
		throw ImageLoadingException(IMG_GetError());
	}

	texture = SDL_CreateTextureFromSurface(renderer.GetHandle(), surface);
	if (!texture)
	{
		SDL_FreeSurface(surface);
		throw ImageLoadingException(SDL_GetError());
	}

	SDL_QueryTexture(texture, nullptr, nullptr, &width, &height);
}

ImageAsset::~ImageAsset()
{
	SDL_DestroyTexture(texture);
}