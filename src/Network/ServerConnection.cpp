#include "ServerConnection.h"

#include <iostream>

#include <NetworkMessages.h>

size_t ServerConnection::packetSize = 512;

static ServerConnection* current = nullptr;

ServerConnectionException::ServerConnectionException(const std::string& msg)
    : std::runtime_error(msg)
{   
}

ServerConnection* ServerConnection::GetCurrent()
{
	return current;
}

void ServerConnection::Update(float dt)
{
	if (SDLNet_UDP_Recv(udpSocket, udpPacket))
	{
		NetworkMessage message;
		TranslateNetworkMessage((char*)udpPacket->data, &message);
		
	}
}

ServerConnection::ServerConnection(const std::string& host, Uint16 port)
{
	if (current == nullptr)
	{
		if (SDLNet_ResolveHost(&remoteAddress, host.c_str(), port))
		{
			throw ServerConnectionException(SDLNet_GetError());
		}

		udpSocket = SDLNet_UDP_Open(0);
		if (!udpSocket)
		{
			throw ServerConnectionException(SDLNet_GetError());
		}

		udpPacket = SDLNet_AllocPacket(packetSize);
		if (!udpPacket)
		{
			throw ServerConnectionException(SDLNet_GetError());
		}

		state = ConnectionState::ConnectedToLobbyServer;
		current = this;
	}
	else
	{
		throw ServerConnectionException("Attempt to create multiple ServerConnection objects.");
	}
}

ServerConnection::~ServerConnection()
{
    SDLNet_FreePacket(udpPacket);
	current = nullptr;
}

LoginError ServerConnection::Login(const std::string& username, const std::string& unencryptedPassword)
{
    constexpr int timeout = 5000; // timeout, in miliseconds
    int timer;

    NetworkMessage message;

    message.dataSize = username.size() + unencryptedPassword.size() + 2;
    message.data = new char[message.dataSize];
    message.type = NetworkMessageType::NETMSG_CTS_LOGINREQ;

    memcpy(message.data, username.c_str(), username.size() + 1);
    memcpy(message.data + username.size() + 1, unencryptedPassword.c_str(), unencryptedPassword.size() + 1);

    udpPacket->data = (Uint8*)GenerateNetworkMessage(&message, &udpPacket->len);
	udpPacket->address = this->remoteAddress;
	
    SDLNet_UDP_Send(udpSocket, -1, udpPacket);

	delete message.data;

	return LoginError::NoError;
}
