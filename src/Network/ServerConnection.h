#pragma once

#include <string>
#include <stdexcept>
#include <functional>

#include <SDL_net.h>

#include <NetworkMessages.h>

enum class LoginError
{
    NoError,
    WrongAccountDetails,
    ConnectionFailure
};

enum class ConnectionState
{
    Disconnected,
    ConnectedToLobbyServer,
    ConnectedToMatch,
    RetryingConnection
};

enum class ProgressState
{
    Idle,
    Attempting,
    Successful,
    Failed
};

class ServerConnectionException : public std::runtime_error
{
public:
    ServerConnectionException(const std::string& msg);
};

class ServerConnection final
{
public:
    ServerConnection(const std::string& host, Uint16 port);
	ServerConnection() = delete;
    ~ServerConnection();

	static ServerConnection* GetCurrent();

	void Update(float dt);

    //
    //  Tries to login to the server using the specified credentials.
    //
    LoginError Login(const std::string& username, const std::string& unencryptedPassword);

    //
    //  Tries to login to the server using the specified credentials.
    //  Login proccess runs in a separate thread.
    //
    void AsyncLogin(const std::string& username, const std::string& unencryptedPassword);

    //
    //  Returns the current progress of the last async login request.
    //
    ProgressState GetAsyncLoginState() const;

    //
    //  Returns last login attempt's error.
    //
    LoginError GetLoginError() const;

    void InterruptAsyncLogin();

    ConnectionState GetState() const;

private:
    static size_t packetSize;

    UDPpacket* udpPacket;
    UDPsocket udpSocket;
    IPaddress remoteAddress;
    ConnectionState state;

	void HandleNetworkMessage(NetworkMessage& message);
};