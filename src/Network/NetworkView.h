#pragma once

#include <SDL.h>
#include <SDL_net.h>

using NetworkIdentity = Uint64;

class DataSender;

class NetworkObject
{
public:
	inline NetworkIdentity GetIdentity() const { return identity; }

	virtual void OnDataReceived(char* data, size_t size);
	virtual void OnDataBeingSent(DataSender& ds);

	NetworkObject();

private:
	NetworkIdentity identity;
};