#include "Font.h"

FontLoadingException::FontLoadingException(const char* msg)
	: std::runtime_error(msg)
{
}

static TTF_Font* LoadFont(const char* path, int ptSize)
{
	TTF_Font* ret = TTF_OpenFont(path, ptSize);
	if (!ret)
	{
		throw FontLoadingException(TTF_GetError());
	}
	return ret;
}

FontAsset::FontAsset(const std::string& path, int ptSize)
	: FontAsset(path.c_str(), ptSize)
{
}

TTF_Font* FontAsset::GetHandle() const
{
	return font;
}

FontAsset::FontAsset(const char* path, int ptSize)
{
	this->path = path;
	this->ptSize = ptSize;
	
	font = LoadFont(path, ptSize);
}

FontAsset::FontAsset(const FontAsset& other)
	: path(other.path), ptSize(other.ptSize)
{
	font = LoadFont(other.path.c_str(), other.ptSize);
}

FontAsset& FontAsset::operator=(const FontAsset& other)
{
	TTF_CloseFont(font);
	font = LoadFont(other.path.c_str(), other.ptSize);
	ptSize = other.ptSize;
	path = other.path;

	return *this;
}

FontAsset::~FontAsset()
{
	TTF_CloseFont(font);
}