#pragma once

#include <SDL.h>

class GameState
{
public:
	static void SetCurrent(GameState* state);
	inline static GameState* GetCurrent() { return currentState; }

	virtual void Start();
	virtual void OnUpdate(float dt);
	virtual void OnRender();
	virtual void OnLateUpdate(float dt);
	virtual void OnEnd();
	virtual void OnApplicationQuit();
	virtual void OnKeyDown(SDL_Keycode key, bool repeat);
	virtual void OnMouseDown(Uint8 button);

private:
	static GameState* currentState;
};