#pragma once

namespace KeyboardUtils
{
	//
	//	Returns true if upper case mode is on.
	//	Caps lock makes this state active.
	//	Shift-stroking a key reverse the current state for that single keystroke.
	//
	bool GetUppercaseState();

} // KeyboardUtils