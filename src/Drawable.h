#pragma once

#include "Transform.h"

class Drawable
{
public:
	Transform* GetTransform();
	const Transform* GetTransform() const;
	void SetTransform(Transform* t);

	virtual void Draw();

private:
	Transform* transform;
};