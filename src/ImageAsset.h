#pragma once

#include <string>
#include <stdexcept>

#include <SDL.h>
#include <SDL_image.h>

class Renderer;

class ImageAsset
{
public:
	inline int GetWidth() const { return width; }
	inline int GetHeight() const { return height; }
	inline const std::string& GetPath() const { return path; }
	inline SDL_Texture* GetTexture() const { return texture; }

	ImageAsset(Renderer& renderer, const char* path);
	ImageAsset(Renderer& renderer, const std::string& path);
	~ImageAsset();

private:
	SDL_Texture* texture;
	std::string path;

	int width;
	int height;
};

class ImageLoadingException : std::runtime_error
{
public:
	inline ImageLoadingException(const char* what)
		:	std::runtime_error(what) { }
};