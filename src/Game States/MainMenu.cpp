#include "MainMenu.h"

#include <iostream>
#include <stdexcept>
#include <sstream>

#include <SDL_net.h>

#include "../Renderer.h"
#include "../UI/UIButton.h"
#include "../Network/ServerConnection.h"
#include "../Assets.h"
#include "../UI/UITextField.h"

namespace GameStates
{
	UITextField* tf;

	void MainMenu::Start() 
	{
		tf = new UITextField();
		tf->SetPosition(0.3f, 0.3f);
	}

	void MainMenu::OnUpdate(float dt)
	{
	}

	void MainMenu::OnKeyDown(SDL_Keycode key, bool repeat)
	{
	}

	void MainMenu::OnMouseDown(Uint8 button)
	{
	}

	void MainMenu::OnLateUpdate(float dt)
	{

	}

	void MainMenu::OnEnd()
	{

	}

	void MainMenu::OnApplicationQuit()
	{

	}
}