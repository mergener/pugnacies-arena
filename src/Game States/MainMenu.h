#pragma once

#include <SDL.h>

#include "../GameState.h"

namespace GameStates
{
	class MainMenu : public GameState
	{
	public:
		void Start() override;
		void OnUpdate(float dt) override;
		void OnLateUpdate(float dt) override;
		void OnEnd() override;
		void OnApplicationQuit() override;
		void OnKeyDown(SDL_Keycode key, bool repeat) override;
		void OnMouseDown(Uint8 button) override;

	private:
	};
}