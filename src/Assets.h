#pragma once

#include "ImageAsset.h"
#include "Text.h"
#include "Font.h"

namespace Assets
{
	void Load();

	namespace Images
	{

	} // Images

	namespace Fonts
	{
		extern FontAsset* Arial16;
		extern FontAsset* Arial24;
	} // Fonts
} // Assets