#include "Window.h"

Window* Window::instance;

Vector2Int Window::GetBounds() const
{
	Vector2Int ret;
	SDL_GetWindowSize(window, &ret.x, &ret.y);
	return ret;
}

void Window::SetDimensions(int w, int h)
{
	SDL_SetWindowSize(window, w, h);
}

void Window::SetDimensions(const Vector2Int& bounds)
{
	SetDimensions(bounds.x, bounds.y);
}

Window::Window(const char* title, int x, int y, int w, int h, Uint32 flags)
{
	window = SDL_CreateWindow(title, x, y, w, h, flags);
	if (!window)
	{
		throw WindowCreationException(SDL_GetError());
	}
}