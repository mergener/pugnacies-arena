#include <iostream>

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_net.h>
#include <SDL_ttf.h>

#include "Renderer.h"
#include "Window.h"
#include "GameState.h"
#include "Game States/MainMenu.h"
#include "Network/ServerConnection.h"
#include "Assets.h"
#include "UI\UIElement.h"

class InitializationException : public std::runtime_error
{
public:
	inline InitializationException(const char* what)
		: std::runtime_error(what) { }
};

static float deltaTime = 0;

static void InitializeInternals()
{
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
	{
		std::cerr << "SDL error\n";
		throw InitializationException(SDL_GetError());
	}

	if (TTF_Init() != 0)
	{
		std::cerr << "TTF error\n";
		throw InitializationException(TTF_GetError());
	}

	if (IMG_Init(IMG_INIT_JPG | IMG_INIT_TIF | IMG_INIT_PNG | IMG_INIT_WEBP) == 0)
	{
		std::cerr << "IMG error\n";
		throw InitializationException(IMG_GetError());
	}
	
	if (SDLNet_Init() != 0)
	{
		std::cerr << "SDLNet error\n";
		throw InitializationException(SDLNet_GetError());
	}
}

static void DeactivateInternals()
{
	SDLNet_Quit();
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}

static bool HandleEvent(const SDL_Event& event) // returns false when the game should exit.
{
	switch (event.type)
	{
		case SDL_MOUSEBUTTONDOWN:
		{
			GameState::GetCurrent()->OnMouseDown(event.button.button);
			UIElement::OnMouseDownAll(event.button.button, event.button.clicks);
			break;
		}

		case SDL_KEYDOWN:
		{
			GameState::GetCurrent()->OnKeyDown(event.key.keysym.sym, event.key.repeat);
			UIElement::OnKeyDownAll(event.key.keysym.sym, event.key.repeat);
			break;
		}

		case SDL_TEXTINPUT:
		{
			UIElement::OnTextInputAll(event.edit.text);
			break;
		}

		case SDL_TEXTEDITING:
		{
			UIElement::OnTextEditAll(event.edit.text, event.edit.length, event.edit.start);
			break;
		}

		case SDL_QUIT:
		{
			return false;
		}
	}
	return true;
}

int main(int argc, char** argv)
{
	try
	{
		InitializeInternals();
		Window window("My window", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280, 720, 0);
		Window::SetMain(&window);
		Renderer renderer(window, -1, SDL_RENDERER_ACCELERATED);
		Renderer::SetMain(&renderer);

		bool running = true;
		SDL_Event event;
		GameState* state;

		Uint32 thisTick = SDL_GetPerformanceCounter();
		Uint32 lastTick = 0;

		Assets::Load();

		state = new GameStates::MainMenu();
		GameState::SetCurrent(state);

		while (running)
		{
			if (SDL_PollEvent(&event))
			{
				running = HandleEvent(event);
			}
			deltaTime = (thisTick - lastTick) / (float)(SDL_GetPerformanceFrequency());
			lastTick = thisTick;
			thisTick = SDL_GetPerformanceCounter();

			state->OnUpdate(deltaTime);
			UIElement::UpdateAll(deltaTime);

			renderer.RenderPresent();
			
			UIElement::RenderAll();
			
			state->OnLateUpdate(deltaTime);
			
			state = GameState::GetCurrent();

			if (ServerConnection::GetCurrent())
			{
				ServerConnection::GetCurrent()->Update(deltaTime);
			}
		}

		state->OnEnd();

		DeactivateInternals();

		return 0;
	}
	catch (const std::runtime_error& error)
	{
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Fatal error", error.what(),	nullptr);

		return -1;
	}
}