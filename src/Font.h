#pragma once

#include <SDL_ttf.h>
#include <string>
#include <stdexcept>

class FontLoadingException : public std::runtime_error
{
public:
	FontLoadingException(const char* msg);
};

class FontAsset
{
public:
	TTF_Font* GetHandle() const;

	FontAsset(const char* path, int ptSize);
	FontAsset(const std::string& path, int ptSize);
	FontAsset(const FontAsset& other);
	FontAsset& operator=(const FontAsset& other);
	~FontAsset();

private:
	TTF_Font* font;
	std::string path;
	int ptSize;
};