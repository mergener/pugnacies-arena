#pragma once

#include <unordered_set>
#include <SDL.h>

class UIElement
{
public:
	//
	//	Bounds' values represent a proportion to the window's dimensions.
	//	For example, a value of 0.3 in maxWidth means that this UIElement's width
	//	occupies 30% of the window's horizontal length.
	//
	struct Bounds
	{
		SDL_Rect GetExactRect() const;
		void GetExactRect(SDL_Rect* outRect) const;
		int GetExactX() const;
		int GetExactY() const;
		int GetExactMaxWidth() const;
		int GetExactMaxHeight() const;

		float posX;
		float posY;
		float maxWidth;
		float maxHeight;
	};

	const Bounds& GetBounds() const;

	bool IsActive() const;
	void SetActive(bool active);

	bool IsFocus() const;
	void BecomeFocus();

	virtual void SetPosition(int x, int y);
	virtual void SetPosition(float x, float y);

	UIElement(const UIElement&) = delete;

	static void RenderAll();
	static void UpdateAll(float dt);
	static void OnMouseDownAll(Uint8 button, Uint8 clicks);
	static void OnMouseUpAll(Uint8 button);
	static void OnKeyDownAll(SDL_Keycode key, bool repeat);
	static void OnTextInputAll(const char* text);
	static void OnTextEditAll(const char* text, int length, int start);

	static UIElement* GetCurrentFocus();
	static void SetFocus(UIElement* element);

protected:
	virtual void OnRender();
	virtual void OnUpdate(float dt);
	virtual void OnMouseDown(Uint8 button, Uint8 clicks);
	virtual void OnMouseUp(Uint8 button);
	virtual void OnKeyDown(SDL_Keycode key, bool repeat);
	virtual void OnTextInput(const char* text);
	virtual void OnTextEdit(const char* text, int length, int start);

	Bounds bounds;

	UIElement();
	~UIElement();

private:
	bool active = true;

	static std::unordered_set<UIElement*> allElements;
	static UIElement* focus;
};
