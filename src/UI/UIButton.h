#pragma once

#include <string>

#include <SDL.h>

#include "UIText.h"
#include "../ImageAsset.h"

class UIButton : public UIElement
{
public:
	using EvHandler = void(*)(UIButton&);

	EvHandler onClick = nullptr;
	EvHandler onHover = nullptr;
	EvHandler onStopHover = nullptr;

	void SetBackgroundImage(const ImageAsset* bgImage);

	const SDL_Color& GetImageColor() const;
	void SetImageColor(const SDL_Color& color);

	const SDL_Color& GetTextColor() const;
	void SetTextColor(const SDL_Color& color);

	const std::string& GetTitle() const;
	void SetTitle(const std::string& text, const FontAsset* font);
	void SetTitle(const std::string& text);
	void SetFont(const FontAsset* font);

	void SetPosition(int x, int y) override;
	void SetPosition(float x, float y) override;

	UIButton();

	UIButton(
		const std::string& title,
		const FontAsset* font,
		const ImageAsset* bgImage,
		EvHandler onClick,
		EvHandler onHover = nullptr,
		EvHandler onStopHover = nullptr);

	UIButton(
		const std::string& title,
		const FontAsset* font,
		float width, float height,
		EvHandler onClick,
		EvHandler onHover = nullptr,
		EvHandler onStopHover = nullptr);

	UIButton(const UIButton& other) = delete;

	~UIButton();

	void OnRender() override;
	void OnUpdate(float dt) override;
	void OnMouseDown(Uint8 button, Uint8 clicks) override;
	void OnMouseUp(Uint8 button) override;

private:
	UIText* textHandle;
	const ImageAsset* image = nullptr;
	SDL_Color imageColor;

	bool isBeingHovered = false;
};