#pragma once

#include <SDL_ttf.h>

#include <string>

#include "UIElement.h"
#include "../Font.h"
#include "../Text.h"

class UIText : public UIElement
{
public:
	UIText(const std::string& textHandle = "Empty Text", const FontAsset* font = nullptr);
	UIText(const UIText& other) = delete;
	UIText& operator=(const UIText& other) = delete;

	const SDL_Color& GetColor() const;
	void SetColor(const SDL_Color& color);

	const std::string& GetString() const;
	void SetString(const std::string& str);
	void SetString(const char* str);
	void AppendString(const std::string& str);
	void AppendString(const char* str);
	char PopCharacter();
	
	const FontAsset* GetFont() const;
	void SetFont(const FontAsset* font);
		
	TextHandle& GetTextHandle();

	void OnRender() override;
	void OnUpdate(float dt) override;
	void OnMouseDown(Uint8 button, Uint8 clicks) override;
	void OnMouseUp(Uint8 button) override;

private:
	TextHandle textHandle;
	SDL_Color color = { 255, 255, 255, 255 };
};