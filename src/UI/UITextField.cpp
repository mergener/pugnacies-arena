#include "UITextField.h"

#include <SDL.h>

#include "../KeyboardUtils.h"
#include "../Renderer.h"

UITextField::UITextField()
{
	contentText = new UIText(" ");
	backgroundText = new UIText("Write here...");
	bounds.maxWidth = 0.4f;
	bounds.maxHeight = 0.3f;
}

UITextField::~UITextField()
{
	delete contentText;
	delete backgroundText;
}

int UITextField::GetMaximumTextLength() const
{
	return maximumTextLength;
}

void UITextField::SetMaximumTextLength(int length)
{
	maximumTextLength = length;
}

const std::regex& UITextField::GetConstraintRegex() const
{
	return constraintsRegex;
}

void UITextField::SetConstraintRegex(const std::regex& regex)
{
	constraintsRegex = regex;
}

const std::string& UITextField::GetContent() const
{
	return contentText->GetString();
}

void UITextField::SetContent(const std::string& str)
{
	if (std::regex_match(str, constraintsRegex))
	{
		GetContentText().SetString(str);
		UpdateContents();
	}
}

const ImageAsset* UITextField::GetBackgroundImage() const
{
	return backgroundImage;
}

void UITextField::SetBackgroundImage(const ImageAsset* bgImage)
{
	backgroundImage = bgImage;
}

UIText& UITextField::GetBackgroundText()
{
	return *backgroundText;
}

UIText& UITextField::GetContentText()
{
	return *contentText;
}

bool UITextField::GetPasswordMode() const
{
	return passwordMode;
}

void UITextField::SetPasswordMode(bool pm)
{
	passwordMode = pm;
}

void UITextField::OnRender()
{
	///
	backgroundText->SetPosition(bounds.posX, bounds.posY);
	contentText->SetPosition(bounds.posX, bounds.posY);
	///

	Renderer* renderer = Renderer::GetInstance();
	SDL_Color oldColor = renderer->GetColor();
	renderer->SetColor(color);

	if (backgroundImage != nullptr)
	{
		renderer->DrawImage(
			*backgroundImage, bounds.GetExactX(), bounds.GetExactY()
		);
	}
	else
	{
		renderer->DrawRectangle(
			bounds.GetExactX(), bounds.GetExactY(),
			bounds.GetExactMaxWidth(), bounds.GetExactMaxHeight()
		);
	}

	renderer->SetColor(oldColor);
}

void UITextField::UpdateContents()
{
	if(onContentChange != nullptr)
		onContentChange(*this);
}

void UITextField::OnMouseDown(Uint8 button, Uint8 clicks)
{
	//	Check if mouse is hovering the input field.
	SDL_Point mousePos;
	SDL_GetMouseState(&mousePos.x, &mousePos.y);

	SDL_Rect rect = {
		bounds.GetExactX(),
		bounds.GetExactY(),
		bounds.GetExactMaxWidth(),
		bounds.GetExactMaxHeight()
	};

	bool isHovering = SDL_PointInRect(&mousePos, &rect);

	if (isHovering)
	{
		// Mouse is hovering, the text should become the focus.
		BecomeFocus();
		backgroundText->SetActive(false);
	}
	else
	{
		if (IsFocus())
		{
			UpdateContents();
			SetFocus(nullptr);
		}
	}
}

void UITextField::OnKeyDown(SDL_Keycode key, bool repeat)
{
	if (!IsFocus())
		return;

	// Handle special characters here.
	if (key == SDLK_BACKSPACE && contentText->GetString().size() > 0)
	{
		// backspace
		contentText->PopCharacter();
		textCursor--;
	}
	else if (key == SDLK_c && (SDL_GetModState() & KMOD_CTRL))
	{
		// ctrl + c
	}
	else if (key == SDLK_v && (SDL_GetModState() & KMOD_CTRL))
	{
		// ctrl + v
		SetContent(SDL_GetClipboardText());
	}
}

void UITextField::OnTextInput(const char* text)
{
	contentText->AppendString(text);
}

void UITextField::OnTextEdit(const char* text, int length, int start)
{
}