#include "UIElement.h"

#include "../Window.h"

std::unordered_set<UIElement*> UIElement::allElements;
UIElement* UIElement::focus;

int UIElement::Bounds::GetExactX() const
{
	return Window::GetInstance()->GetBounds().x * posX;
}

int UIElement::Bounds::GetExactY() const
{
	return Window::GetInstance()->GetBounds().y * posY;
}

int UIElement::Bounds::GetExactMaxWidth() const
{
	return Window::GetInstance()->GetBounds().x * maxWidth;
}

int UIElement::Bounds::GetExactMaxHeight() const
{
	return Window::GetInstance()->GetBounds().y * maxHeight;
}

const UIElement::Bounds& UIElement::GetBounds() const
{
	return bounds;
}

SDL_Rect UIElement::Bounds::GetExactRect() const
{
	return SDL_Rect {
		GetExactX(),
		GetExactY(),
		GetExactMaxWidth(),
		GetExactMaxHeight()
	};
}

void UIElement::Bounds::GetExactRect(SDL_Rect* outRect) const
{
	outRect->x = GetExactX();
	outRect->y = GetExactY();
	outRect->w = GetExactMaxWidth();
	outRect->h = GetExactMaxHeight();
}

UIElement::UIElement()
{	
	allElements.emplace(this);
}

UIElement::~UIElement()
{
	allElements.erase(this);
	if (IsFocus())
	{
		SetFocus(nullptr);
	}
}

bool UIElement::IsFocus() const
{
	return focus == this;
}

void UIElement::BecomeFocus()
{
	SetFocus(this);
	printf("New focus gained!\n");
}

UIElement* UIElement::GetCurrentFocus()
{
	return focus;
}

void UIElement::SetFocus(UIElement* el)
{
	focus = el;
}

void UIElement::RenderAll()
{
	for (auto ue : allElements)
	{
		if (ue->IsActive())
			ue->OnRender();
	}
}

void UIElement::UpdateAll(float dt)
{
	for (auto ue : allElements)
	{
		if (ue->IsActive())
			ue->OnUpdate(dt);
	}
}

void UIElement::OnMouseDownAll(Uint8 button, Uint8 clicks)
{
	for (auto ue : allElements)
	{
		if (ue->IsActive())
			ue->OnMouseDown(button, clicks);
	}
}

void UIElement::OnMouseUpAll(Uint8 button)
{
	for (auto ue : allElements)
	{
		if (ue->IsActive())
			ue->OnMouseUp(button);
	}
}

void UIElement::OnKeyDownAll(SDL_Keycode key, bool repeat)
{
	for (auto ue : allElements)
	{
		if (ue->IsActive())
			ue->OnKeyDown(key, repeat);
	}
}

void UIElement::OnTextInputAll(const char* text)
{
	for (auto ue : allElements)
	{
		if (ue->IsActive())
			ue->OnTextInput(text);
	}
}

void UIElement::OnTextEditAll(const char* text, int length, int start)
{
	for (auto ue : allElements)
	{
		if (ue->IsActive())
			ue->OnTextEdit(text, length, start);
	}
}

void UIElement::SetPosition(int x, int y)
{
	Vector2Int winDim = Window::GetInstance()->GetBounds();
	bounds.posX = (float)x/(float)winDim.x;
	bounds.posY = (float)y/(float)winDim.y;
}

void UIElement::SetPosition(float x, float y)
{
	bounds.posX = x;
	bounds.posY = y;
}

bool UIElement::IsActive() const
{
	return active;
}

void UIElement::SetActive(bool active)
{
	this->active = active;
}

void UIElement::OnRender()
{
}

void UIElement::OnUpdate(float dt)
{
}

void UIElement::OnMouseDown(Uint8 button, Uint8 clicks)
{
}

void UIElement::OnMouseUp(Uint8 button)
{
}

void UIElement::OnKeyDown(SDL_Keycode key, bool repeat)
{
}

void UIElement::OnTextInput(const char* text)
{
}

void UIElement::OnTextEdit(const char* text, int length, int start)
{
}