#pragma once

#include "UIElement.h"
#include "../ImageAsset.h"

class UIImage : public UIElement
{
public:
	UIImage() = delete;
	UIImage(const ImageAsset* asset);
	UIImage(const UIImage& other);
	UIImage& operator=(const UIImage& other);

	const SDL_Color& GetColor() const;
	void SetColor(const SDL_Color& col);
	void SetColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a = 255);

protected:
	void OnRender() override;
	void OnUpdate(float dt) override;
	void OnMouseDown(Uint8 button, Uint8 clicks) override;
	void OnMouseUp(Uint8 button) override;

private:
	const ImageAsset* imgAsset;
	SDL_Color color;
};