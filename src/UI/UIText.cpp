#include "UIText.h"

#include "../Renderer.h"

UIText::UIText(const std::string& textHandle, const FontAsset* font)
	: textHandle(textHandle, font)
{

}

void UIText::OnRender()
{
	Renderer::GetInstance()->DrawText(textHandle, bounds.GetExactX(), bounds.GetExactY());
}

void UIText::OnUpdate(float dt)
{

}

void UIText::OnMouseDown(Uint8 button, Uint8 clicks)
{

}

void UIText::OnMouseUp(Uint8 button)
{

}

const SDL_Color& UIText::GetColor() const
{
	return color;
}

void UIText::SetColor(const SDL_Color& color)
{
	this->color = color;
}

const std::string& UIText::GetString() const
{
	return textHandle.GetString();
}

void UIText::SetString(const std::string& str)
{
	textHandle.~TextHandle();
	new (&textHandle) TextHandle(str, textHandle.GetFont());
}

void UIText::SetString(const char* str)
{
	textHandle.~TextHandle();
	new (&textHandle) TextHandle(str, textHandle.GetFont());
}

void UIText::AppendString(const char* str)
{
	textHandle.~TextHandle();
	new (&textHandle) TextHandle(textHandle.GetString() + str, textHandle.GetFont());
}

void UIText::AppendString(const std::string& str)
{
	AppendString(str.c_str());
}

char UIText::PopCharacter()
{
	const std::string& str = GetString();
	int index = str.size() - 1;
	char ret = str[index];
	textHandle.~TextHandle();
	new (&textHandle) TextHandle(str.substr(0, index) , textHandle.GetFont());
	return ret;
}

const FontAsset* UIText::GetFont() const
{
	return textHandle.GetFont();
}

void UIText::SetFont(const FontAsset* font)
{
	textHandle.~TextHandle();
	new (&textHandle) TextHandle(GetString(), font);
}

TextHandle& UIText::GetTextHandle()
{
	return textHandle;
}