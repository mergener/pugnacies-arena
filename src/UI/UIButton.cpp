#include "UIButton.h"

#include "../Renderer.h"

UIButton::UIButton()
	: UIButton("Button", nullptr, 0.2, 0.15, nullptr, nullptr, nullptr)
{

}

const SDL_Color& UIButton::GetImageColor() const
{
	return imageColor;
}

void UIButton::SetImageColor(const SDL_Color& color)
{
	imageColor = color;
}

void UIButton::SetBackgroundImage(const ImageAsset* bgImage)
{
	image = bgImage;
}

const SDL_Color& UIButton::GetTextColor() const
{
	return textHandle->GetColor();
}

void UIButton::SetTextColor(const SDL_Color& color)
{
	textHandle->SetColor(color);
}

const std::string& UIButton::GetTitle() const
{
	return textHandle->GetString();
}

void UIButton::SetTitle(const std::string& text, const FontAsset* font)
{
	textHandle->SetString(text);
	textHandle->SetFont(font);
}

void UIButton::SetTitle(const std::string& text)
{
	textHandle->SetString(text);
}

void UIButton::SetFont(const FontAsset* font)
{
	textHandle->SetFont(font);
}

void UIButton::OnRender()
{
	Renderer* renderer = Renderer::GetInstance();
	Bounds b = GetBounds();
	SDL_Color prevColor = renderer->GetColor();

	renderer->SetColor(imageColor.r, imageColor.g, imageColor.b, imageColor.a);
	if (image != nullptr)
	{
		renderer->DrawImage(*image, b.GetExactX(), b.GetExactY());
	}
	else
	{
		renderer->FillRectangle(b.GetExactX(), b.GetExactY(), b.GetExactMaxWidth(), b.GetExactMaxHeight());
	}
	renderer->SetColor(prevColor);
	textHandle->OnRender();
}

void UIButton::OnUpdate(float dt)
{
	textHandle->OnUpdate(dt);
	SDL_Point mousePos;
	SDL_GetMouseState(&mousePos.x, &mousePos.y);
	auto bounds = GetBounds();
	SDL_Rect rect = { bounds.GetExactX(), bounds.GetExactY(),
	bounds.GetExactMaxWidth(), bounds.GetExactMaxHeight() };
	
	if (SDL_PointInRect(&mousePos, &rect))
	{
		if (!isBeingHovered)
		{
			isBeingHovered = true;
			if (onHover)
				onHover(*this);
		}
	}
	else
	{
		if (isBeingHovered)
		{
			isBeingHovered = false;
			if (onStopHover)
				onStopHover(*this);
		}
	}
}

void UIButton::OnMouseDown(Uint8 button, Uint8 clicks)
{
	textHandle->OnMouseDown(button, clicks);
	if (isBeingHovered && button == SDL_BUTTON_LEFT)
	{
		onClick(*this);
	}
}

void UIButton::OnMouseUp(Uint8 button)
{
	textHandle->OnMouseUp(button);
}

UIButton::UIButton(
	const std::string& title,
	const FontAsset* font,
	const ImageAsset* bgImage,
	EvHandler onClick,
	EvHandler onHover,
	EvHandler onStopHover)
	: onClick(onClick), onHover(onHover), onStopHover(onStopHover)
{
	textHandle = new UIText(title, font);
	SetPosition(this->bounds.posX, this->bounds.posY);
	textHandle->GetTextHandle().alignment = TextHandle::AlignMode::Center;
	
	SDL_assert(bgImage != nullptr);

	bounds.maxHeight = (float)bgImage->GetWidth() / Window::GetInstance()->GetBounds().x;
	bounds.maxHeight = (float)bgImage->GetHeight() / Window::GetInstance()->GetBounds().y;
}

void UIButton::SetPosition(int x, int y)
{
	UIElement::SetPosition(x, y);
	textHandle->SetPosition(
		bounds.posX + bounds.maxWidth / 2,
		bounds.posY + bounds.maxHeight / 2 - (textHandle->GetTextHandle().GetHeight() / (float)(2 * Window::GetInstance()->GetBounds().y)));
}

void UIButton::SetPosition(float x, float y)
{
	UIElement::SetPosition(x, y);
	textHandle->SetPosition(
		bounds.posX + bounds.maxWidth / 2, 
		bounds.posY + bounds.maxHeight / 2 - (textHandle->GetTextHandle().GetHeight() / (float)(2 * Window::GetInstance()->GetBounds().y)));
}

UIButton::UIButton(
	const std::string& title,
	const FontAsset* font,
	float width, float height,
	EvHandler onClick,
	EvHandler onHover,
	EvHandler onStopHover)
	: onClick(onClick), onHover(onHover), onStopHover(onStopHover)
{
	textHandle = new UIText(title, font);
	textHandle->GetTextHandle().alignment = TextHandle::AlignMode::Center;
	bounds.maxWidth = width;
	bounds.maxHeight = height;
	SetPosition(this->bounds.posX, this->bounds.posY);
}

UIButton::~UIButton()
{
	delete textHandle;
}
