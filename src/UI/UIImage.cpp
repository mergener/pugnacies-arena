#include "UIImage.h"

#include "../Renderer.h"

UIImage::UIImage(const ImageAsset* asset)
	: imgAsset(asset)
{
}

UIImage::UIImage(const UIImage& other)
	: imgAsset(other.imgAsset), color(other.color)
{
}

UIImage& UIImage::operator=(const UIImage& other)
{
	return *this;
}

const SDL_Color& UIImage::GetColor() const
{
	return color;
}

void UIImage::SetColor(const SDL_Color& col)
{
	color = col;
}

void UIImage::SetColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
	color.r = r;
	color.g = g;
	color.b = b;
	color.a = a;
}

void UIImage::OnRender()
{
	Renderer* renderer = Renderer::GetInstance();

	SDL_Color prevColor = renderer->GetColor();

	renderer->SetColor(this->color);
	renderer->DrawImage(*imgAsset,
		bounds.GetExactX(),
		bounds.GetExactY());

	renderer->SetColor(prevColor);
}

void UIImage::OnUpdate(float dt) 
{
}

void UIImage::OnMouseDown(Uint8 button, Uint8 clicks) 
{
}

void UIImage::OnMouseUp(Uint8 button) 
{
}