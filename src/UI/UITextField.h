#pragma once

#include <string>
#include <regex>

#include "UIElement.h"
#include "UIText.h"
#include "../Font.h"
#include "../ImageAsset.h"

class UITextField : public UIElement
{
public:
	using ContentChangeCallback = void(*)(UITextField&);
	
	//
	//	Called whenever the user types anything into this UITextField.
	//
	ContentChangeCallback onContentChange;
	
	int GetMaximumTextLength() const;
	void SetMaximumTextLength(int length);

	const std::regex& GetConstraintRegex() const;
	void SetConstraintRegex(const std::regex& regex);

	//
	//	The content written by the user.
	//	Shorthand for GetContentText().GetString()
	//
	const std::string& GetContent() const;
	void SetContent(const std::string& str);

	const ImageAsset* GetBackgroundImage() const;
	void SetBackgroundImage(const ImageAsset* img);

	UIText& GetBackgroundText();
	//
	//	The text that contains user input.
	//
	UIText& GetContentText();

	//
	//	When in password mode, asterisks will be shown instead of displaying
	//	the true characters of the string.
	//
	bool GetPasswordMode() const;
	void SetPasswordMode(bool pm);

	UITextField();
	UITextField(const UITextField& other) = delete;
	UITextField& operator=(const UITextField& other) = delete;
	~UITextField();

protected:	
	void OnRender() override;
	void OnMouseDown(Uint8 button, Uint8 clicks) override;
	void OnKeyDown(SDL_Keycode key, bool repeat);
	void OnTextInput(const char* text) override;
	void OnTextEdit(const char* text, int length, int start) override;

private:
	UIText* contentText;
	UIText* backgroundText;

	const ImageAsset* backgroundImage = nullptr;
	SDL_Color color = { 255, 255, 255, 255 };

	bool passwordMode = false;
	bool isBeingHovered = false;
	bool isBeingFocused = false;

	int maximumTextLength = 40;
	int textCursor = 0;
	std::regex constraintsRegex;

	void UpdateContents();
};