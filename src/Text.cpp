#include "Text.h"

#include "Renderer.h"
#include "Assets.h"

#define DEFAULT_FONT (Assets::Fonts::Arial16)

TextCreationException::TextCreationException(const char* msg)
	: std::runtime_error(msg)
{
}

TextHandle::TextHandle(const std::string& textHandle, const FontAsset* font)
	: textHandle(textHandle), font( (font != nullptr) ? font : DEFAULT_FONT )
{
	SDL_Surface* surface = TTF_RenderText_Blended(this->font->GetHandle(), textHandle.c_str(), SDL_Color { 255, 255, 255, 255});
	
	if (!surface)
	{
		throw TextCreationException(SDL_GetError());
	}

	texture = SDL_CreateTextureFromSurface(Renderer::GetInstance()->GetHandle(), surface);
	if (!texture)
	{
		SDL_FreeSurface(surface);
		throw TextCreationException(SDL_GetError());
	}

	SDL_QueryTexture(texture, nullptr, nullptr, &width, &height);
	SDL_FreeSurface(surface);
}

TextHandle::TextHandle(const TextHandle& other)
	: TextHandle(other.textHandle, other.font)
{
	this->alignment = other.alignment;
}

TextHandle& TextHandle::operator=(const TextHandle& other)
{
	SDL_DestroyTexture(texture);

	this->textHandle = other.textHandle;
	this->font = (other.font != nullptr) ? other.font : DEFAULT_FONT;
	this->alignment = other.alignment;

	SDL_Surface* surface = TTF_RenderText_Blended(font->GetHandle(), textHandle.c_str(), SDL_Color { 255, 255, 255, 255 });

	if (!surface)
	{
		throw TextCreationException(SDL_GetError());
	}

	texture = SDL_CreateTextureFromSurface(Renderer::GetInstance()->GetHandle(), surface);
	if (!texture)
	{
		SDL_FreeSurface(surface);
		throw TextCreationException(SDL_GetError());
	}

	SDL_QueryTexture(texture, nullptr, nullptr, &width, &height);
	SDL_FreeSurface(surface);

	return *this;
}

TextHandle::~TextHandle()
{
	SDL_DestroyTexture(texture);
}

int TextHandle::GetWidth() const
{
	return width;
}

int TextHandle::GetHeight() const
{
	return height;
}

SDL_Texture* TextHandle::GetTexture() const
{
	return texture;
}

const std::string& TextHandle::GetString() const
{
	return textHandle;
}

const FontAsset* TextHandle::GetFont() const
{
	return font;
}