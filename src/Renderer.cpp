#include "Renderer.h"

Renderer* Renderer::instance = nullptr;

Renderer::Renderer(Window& window, int index, Uint32 flags, SDL_BlendMode blendMode)
{
	renderer = SDL_CreateRenderer(window.GetHandle(), index, flags);
	
	if (!renderer)
	{
		throw RendererCreationException(SDL_GetError());
	}
	this->window = &window;
	SDL_RenderClear(renderer);
	SDL_SetRenderDrawBlendMode(renderer, blendMode);
}

void Renderer::DrawRectangle(int x, int y, int w, int h)
{
	SDL_Rect rect = { x, y, w, h };
	SDL_RenderDrawRect(renderer, &rect);
}

void Renderer::DrawCircle(int x, int y, int radius)
{
	
}

void Renderer::FillCircle(int x, int y, int radius)
{
	
}

void Renderer::FillRectangle(int x, int y, int w, int h)
{
	SDL_Rect rect = { x, y, w, h };
	SDL_RenderFillRect(renderer, &rect);
}

void Renderer::DrawImage(const ImageAsset& img, int x, int y)
{
	SDL_Rect dstRect = { x, y, img.GetWidth(), img.GetHeight() };
	SDL_RenderCopy(renderer, img.GetTexture(), nullptr, &dstRect);
}

void Renderer::DrawImage(const ImageAsset& img, int x, int y, int qx, int qy, int qw, int qh)
{
	SDL_Rect dstRect = { x, y, img.GetWidth(), img.GetHeight() };
	SDL_Rect srcRect = { qx, qy, qw, qh };
	SDL_RenderCopy(renderer, img.GetTexture(), &srcRect, &dstRect);
}

void Renderer::DrawText(const TextHandle& textHandle, int x, int y)
{
	SDL_Rect dstRect;

	switch (textHandle.alignment)
	{
		case TextHandle::AlignMode::Left:
			dstRect = { x, y, textHandle.GetWidth(), textHandle.GetHeight() };
			break;

		case TextHandle::AlignMode::Center:
			dstRect = { x - textHandle.GetWidth() / 2, y, textHandle.GetWidth(), textHandle.GetHeight() };
			break;

		case TextHandle::AlignMode::Right:
			dstRect = { x - textHandle.GetWidth(), y, textHandle.GetWidth(), textHandle.GetHeight() };
			break;
	}
	
	SDL_RenderCopy(renderer, textHandle.GetTexture(), nullptr, &dstRect);
}

void Renderer::SetColor(const SDL_Color& color)
{
	SetColor(color.r, color.g, color.b, color.a);
}

void Renderer::SetColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
	SDL_SetRenderDrawColor(renderer, r, g, b, a);
	color.r = r;
	color.g = g;
	color.b = b;
}

void Renderer::SetBackgroundColor(Uint8 r, Uint8 g, Uint8 b)
{
	backgroundColor.r = r;
	backgroundColor.g = g;
	backgroundColor.b = b;
}

SDL_Color Renderer::GetColor() const
{
	return color;
}

SDL_Color Renderer::GetBackgroundColor() const
{
	return backgroundColor;
}

const Window& Renderer::GetWindow() const
{
	return *window;
}

void Renderer::RenderPresent()
{
	SDL_RenderPresent(renderer);
	SDL_SetRenderDrawColor(renderer, backgroundColor.r, backgroundColor.g, backgroundColor.b, 255);
	SDL_RenderClear(renderer);
}
